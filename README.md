# Solana Pay Website

### Local Development

1. Run `yarn` at the root of the directory to install the required packages
2. Run `yarn start` to start the project
3. Start coding!

### **What is the .vscode Folder?**

If you use VSCode to build your app, we included a list of suggested extensions that will help you build this project! Once you open this project in VSCode, you will see a popup asking if you want to download the recommended extensions.

### About

I'm building this project as a part of my buildspace.so journey.
Be sure to check out the lessons over @
[Buildspace: Solana Pay](https://buildspace.so/p/build-solana-web3-app/) if you're interested in learing more about web3 development in a fun and interactive way.
